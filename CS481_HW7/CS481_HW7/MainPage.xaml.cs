﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using ModernHttpClient;
using Plugin.Connectivity;
using Xamarin.Forms;
using QuickType;

namespace CS481_HW7
{
	public partial class MainPage : ContentPage
	{
        event ConnectivityChangedEventHandler ConnectivityChanged;
        public MainPage()
        {
            InitializeComponent();
            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    //Popup notification that user has no internet connection active.
                    await DisplayAlert("Disconnected", "You have lost your internet connection.\nPlease reconnect to continue using this application.", "OK");
                }
            };
        }
        public async void HandleWord(object sender, System.EventArgs e)
        {
            //Button activity
            if (CrossConnectivity.Current.IsConnected)
            {
                var client = new HttpClient(new NativeMessageHandler());
                string s = FormatInput(EntryField.Text);
                var uri = new Uri(
                    string.Format($"https://owlbot.info/api/v2/dictionary/" + s));

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                HttpResponseMessage response = await client.SendAsync(request);
                Dictionary[] dictionary = null;
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    dictionary = Dictionary.FromJson(content);
                    if(dictionary == null || content == "[]")
                    {
                        //Rare cases where a code 200 accompanied empty data
                        TypeLabel.Text = "Error: Query did not return usable data.";
                        DefLabel.Text = "";
                        SentLabel.Text = "";
                    }
                    else
                    {
                        TypeLabel.Text = $"The type of the word is {dictionary[0].Type}";
                        DefLabel.Text = $"Definition: {dictionary[0].Definition}";
                        SentLabel.Text = $"Example: {dictionary[0].Example}";
                    }
                }
                else
                {
                    //If the response code is not 200
                    TypeLabel.Text = "Error: No corresponding result.";
                    DefLabel.Text = "";
                    SentLabel.Text = "";
                }
            }
            else
            {
                //If no current connection exists
                TypeLabel.Text = "Error: please connect to the internet and try again.";
                DefLabel.Text = "";
                SentLabel.Text = "";
            }
            
        }

        public string FormatInput(string input)
        {
            string output = "";
            foreach(char c in input)
            {
                //Filter out the first word entered
                if(c == ' ')
                {
                    //The API does not accept spaces, and spaces do not exist in words.
                    break;
                } else
                {
                    output += c;
                }
            }
            //Format for the api (all lowercase)
            output = input.ToLower();
            return output;
        }

        public class ConnectivityChangedEventArgs : EventArgs
        {
            public bool IsConnected { get; set; }
        }

        public delegate void ConnectivityChangedEventHandler(object sender, ConnectivityChangedEventArgs e);
    }
}
